import { RegiaoPage } from './../regiao/regiao';
import { Monstro } from './../../models/monstro';
import { Regiao } from './../../models/regiao';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { SoniPage } from '../soni/soni';

@Component({
  selector: 'page-mundo',
  templateUrl: 'mundo.html'
})
export class MundoPage {
  regioes: Regiao[];

  constructor(public navCtrl: NavController) {
    let aranhaDaFloresta: Monstro = {
      nome: "Aranha da Floresta",
      descricao: "Aranha pequena que vive na floresta. Monstro fraco que não apresenta perigo nem para os iniciantes.",
      nivel: 1
    };

    let regiaoSoni: Regiao = {
      nome: "Soni",
      nivelInicial: 1,
      nivelFinal: 9,
      cor: "light",
      monstros: [aranhaDaFloresta]
    };

    let regiaoDeka: Regiao = {
      nome: "Deka",
      nivelInicial: 10,
      nivelFinal: 19,
      cor: "primary",
      monstros: []
    };

    let regiaoGlaka: Regiao = {
      nome: "Glaka",
      nivelInicial: 20,
      nivelFinal: 29,
      cor: "secondary",
      monstros: []
    };

    let regiaoWalo: Regiao = {
      nome: "Walo",
      nivelInicial: 30,
      nivelFinal: 39,
      cor: "danger",
      monstros: []
    };

    let regiaoDoria: Regiao = {
      nome: "Doria",
      nivelInicial: 40,
      nivelFinal: 50,
      cor: "dark",
      monstros: []
    };

    this.regioes = [
      regiaoSoni,
      regiaoDeka,
      regiaoGlaka,
      regiaoWalo,
      regiaoDoria
    ];
  }

  goToRegiao(regiaoSelecionada: Regiao) {
    this.navCtrl.push(RegiaoPage, {
      regiao: regiaoSelecionada
    });
  }

}
